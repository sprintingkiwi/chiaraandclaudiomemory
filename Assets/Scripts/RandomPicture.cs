﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPicture : MonoBehaviour
{
    public Sprite[] randomSprites;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(SetRandomPicture());
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Debug.Log(transform.position.magnitude);
        if (transform.position.magnitude < 45)
            transform.Translate(Input.acceleration.x, Input.acceleration.y, 0);
        else
            transform.Translate(-transform.position.normalized);
    }

    IEnumerator SetRandomPicture()
    {
        while (true)
        {
            GetComponent<SpriteRenderer>().sprite = randomSprites[Random.Range(0, randomSprites.Length)];

            yield return new WaitForSeconds(Random.Range(5, 10));
        }
    }
}
