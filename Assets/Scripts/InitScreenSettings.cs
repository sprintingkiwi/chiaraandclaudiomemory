﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitScreenSettings : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
        // Screen settings
        if (Application.platform == RuntimePlatform.Android)
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }
    }
}
