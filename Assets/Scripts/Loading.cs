﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    public string newScene;
    public float forcedPause;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(LoadNewScene());
	}
	
	IEnumerator LoadNewScene()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(newScene);
        async.allowSceneActivation = false;

        yield return new WaitForSeconds(forcedPause);

        async.allowSceneActivation = true;
        while (!async.isDone)
        {
            yield return null;
        }
    }
}
